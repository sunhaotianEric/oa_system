package cn.gson.oasys.model.entity.excel;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
@Table(name = "demo")
public class Excel {

    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Id
    Integer id;

    String userName;

    String account;

    String password;

    public Excel() {
    }

    public Excel(String userName, String account, String password) {
        this.userName = userName;
        this.account = account;
        this.password = password;
    }
}
