package cn.gson.oasys.services.file.impl;

import cn.gson.oasys.model.dao.exceldao.ExcelDao;
import cn.gson.oasys.model.entity.excel.Excel;
import cn.gson.oasys.services.file.ExcelUploadService;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

@Service
public class ExcelUploadImpl implements ExcelUploadService {
    @Autowired
    ExcelDao excelDao;

    @Override
    public String readExcelFile(MultipartFile file) {
     String result="";
    //创建处理EXCEL的类  
      ReadExcel readExcel=new ReadExcel();
    //解析excel，获取的事件单  
    List<Excel> useList=readExcel.getExcelInfo(file);
    //至此已经将excel中的数据转换到list里面了,接下来就可以操作list,可以进行保存到数据库,或者其他操作,  
    //和你具体业务有关,这里不做具体的示范  
    if(useList!=null&&!useList.isEmpty()){
        new Thread(){
            @Override
            public void run() {
                excelDao.save(useList);
            }
        }.start();

    }else{
    result="上传失败";
}
return result;
}





}
